const fs = require('fs');
const path = require("path");

module.exports.Convert = (function () {
    //const fs = fileSystem;
    const base64_encode = function (file) {
        // read binary data
        var bitmap = fs.readFileSync(file);
        // convert binary data to base64 encoded string
        return Buffer.from(bitmap).toString('base64');
    }

    // function to create file from base64 encoded string
    const base64_decode = function (base64str, file) {
        // create buffer object from base64 encoded string, it is important to tell the constructor that the string is base64 encoded
        var bitmap = new Buffer(base64str, 'base64');
        // write buffer to file
        fs.writeFileSync(file, bitmap);
    }

    const base64_encodeFromDir = function (pathToDir) {
        if (pathToDir === null || pathToDir === undefined || pathToDir === "") {
            pathToDir = __dirname;
        }
        const images = [];
        const list = fs.readdirSync(pathToDir);

        list.forEach((file) => {
            images.push({
                image: base64_encode(path.join(pathToDir, file)),
                name: file
            });
        });
        return images;
    }

    const toBase64 = function(string){

        return Buffer.from(string).toString('base64');
    }

    return {
        Encode: base64_encode,
        Decode: base64_decode,
        EncodeFromDir: base64_encodeFromDir,
        ToBase64: toBase64
    }
})();