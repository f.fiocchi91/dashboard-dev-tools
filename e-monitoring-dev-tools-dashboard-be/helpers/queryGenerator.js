module.exports.Query = (function () {

    function Query(id, name, description, order, image, category, isActive) {
        this.ID = id;
        this.Name = name;
        this.Description = description;
        this.Order = order;
        this.Image = image;
        this.Category = category;
        this.IsActive = isActive;
    }

    Query.prototype.generate = function () {
        if (this.Image === null) {
            return "DECLARE @widgetID"+ this.ID +" INT = " + this.ID + ";" + String.fromCharCode(13) +
                "DECLARE @imageID" + this.ID + " NVARCHAR(MAX) = NULL;" + String.fromCharCode(13) + String.fromCharCode(13) +
                "UPDATE dbo.DashboardWidgets SET " + String.fromCharCode(13) +
                "[Name] = '" + this.Name + "', " + String.fromCharCode(13) +
                "[Description] = '" + this.Description + "', " + String.fromCharCode(13) +
                "[IsActive] = " + this.IsActive + ", " + String.fromCharCode(13) +
                "[Order] = " + this.Order + ", " + String.fromCharCode(13) +
                "[Category] = " + this.Category + "" + String.fromCharCode(13) +
                "WHERE [ID] = " + this.ID + "; " + String.fromCharCode(13) + String.fromCharCode(13) +

                "UPDATE dbo.DashboardWidgetImages SET" + String.fromCharCode(13) +
                "[Image] = @imageID" + this.ID + ", " + String.fromCharCode(13) +
                "[WidgetViewTypeID] = 1" + String.fromCharCode(13) +
                "WHERE [WidgetID] = @widgetID"+ this.ID + String.fromCharCode(13) + String.fromCharCode(13);
        } else {
            this.Description = this.Description.replace("'", "''");
            this.Name = this.Name.replace("'", "''");
            return "DECLARE @widgetID" + this.ID + " INT = " + this.ID + ";" + String.fromCharCode(13) +
                "DECLARE @imageID" + this.ID + " NVARCHAR(MAX) = '" + this.Image + "';" + String.fromCharCode(13) + String.fromCharCode(13) +
                "UPDATE dbo.DashboardWidgets SET " + String.fromCharCode(13) +
                "[Name] = '" + this.Name + "', " + String.fromCharCode(13) +
                "[Description] = '" + this.Description + "', " + String.fromCharCode(13) +
                "[IsActive] = " + this.IsActive + ", " + String.fromCharCode(13) +
                "[Order] = " + this.Order + ", " + String.fromCharCode(13) +
                "[Category] = " + this.Category + "" + String.fromCharCode(13) +
                "WHERE [ID] = @widgetID"+ this.ID +"; " + String.fromCharCode(13) + String.fromCharCode(13) +

                "UPDATE dbo.DashboardWidgetImages SET" + String.fromCharCode(13) +
                "[Image] = @imageID" + this.ID + ", " + String.fromCharCode(13) +
                "[WidgetViewTypeID] = 1" + String.fromCharCode(13) +
                "WHERE [WidgetID] = @widgetID"+ this.ID +";" + String.fromCharCode(13) + String.fromCharCode(13);
        }
    };

    Query.all = function (array) {
        const queries = [];
        array.forEach(element => {
            queries.push(element.generate())
        });

        return queries;
    }
    //Query.prototype.remove

    return Query;
})();

module.exports.QueryForImage = (function () {
    function QueryForImage(id, image) {
        this.ID = id;
        this.Image = image;
    }
    QueryForImage.prototype.generate = function () {
        return "DECLARE @imageID" + this.ID + " NVARCHAR(MAX) = '" + this.Image + "';" + String.fromCharCode(13) + String.fromCharCode(13) +
            "UPDATE dbo.DashboardWidgets SET " + String.fromCharCode(13) +
            "[Image] = @imageID" + this.ID + "" + String.fromCharCode(13) +
            "WHERE [ID] = " + this.ID + "; " + String.fromCharCode(13) + String.fromCharCode(13);
    };

    return QueryForImage
})();

