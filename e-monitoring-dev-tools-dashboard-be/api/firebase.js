const firebase = require('firebase');

const config = {
  apiKey: "AIzaSyCBYfatk4f8JRJTakRI59KCHC-IkubdgFc",
  authDomain: "dashboard-dev-tools-db.firebaseapp.com",
  databaseURL: "https://dashboard-dev-tools-db.firebaseio.com",
  projectId: "dashboard-dev-tools-db",
  storageBucket: "dashboard-dev-tools-db.appspot.com",
  messagingSenderId: "334474509238",
  appId: "1:334474509238:web:d735a89cd16c724a929f17",
  measurementId: "G-MW0EXBCSPB"
};


firebase.initializeApp(config);
const db = firebase.app().database();

var provider = new firebase.auth.GoogleAuthProvider();


module.exports.db = db;
module.exports.firebase = firebase;