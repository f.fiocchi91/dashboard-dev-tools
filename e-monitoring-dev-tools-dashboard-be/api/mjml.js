const fs = require('fs');
const path = require("path");
const https = require('https');
const util = require('util');

const config = {
    "APPLICATION_ID": "256102e5-20d0-4b53-a78e-290f2640b857",
    "PUBLIC_KEY": "5184dac9-5cf1-4c64-ad6e-524703e5b549",
    "SECRET_KEY": "6b911042-1909-4695-bede-3741f643a870",
    "API_ENDPOINT": "https://api.mjml.io/v1"
}

const templates = {
    "BASIC": "basic.mjml"
}

module.exports.MJMLManager = (function () {

    const render = function (template) {

        return new Promise((resolve, reject) => {
            
            const headers = {
                'Authorization': 'Basic ' + Buffer.from(config.APPLICATION_ID + ":" + config.SECRET_KEY).toString('base64')
            };
            
            const options = {
                // hostname: config.API_ENDPOINT,
                // path: '/render',
                method: 'POST',
                headers: headers
            };

            const mjmlTemplate = fs.readFileSync(path.join(process.cwd(), "mail", templates.BASIC)).toString();

            const data = JSON.stringify({
                "mjml": mjmlTemplate
            });

            let all = '';

            const request = https.request(config.API_ENDPOINT + '/render', options, function (response) {

                response.on('data', (data) => {
                    all = all + data.toString();
                });
                response.on('end', () => {
                    resolve(JSON.parse(all));
                });
                response.on('error', (error) => {
                    reject(error)
                });
            })

            request.write(data);

            request.end();
        });
    }

    return {
        render: render
    }
})();