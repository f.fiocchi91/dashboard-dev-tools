const {google} = require("googleapis");
const OAuth2 = google.auth.OAuth2;

const googleConfig = {
    "CLIENT_ID": "927709975555-2t67ofijqhfss9rl8kjhg3md5khvhn4b.apps.googleusercontent.com",
    "SECRED_ID": "0uPQxLCRcZoXVfFvg-wBjqMs",
    "REFRESH_TOKEN": "1//04YxP6LTTBehHCgYIARAAGAQSNwF-L9IrAzQXQLKguXB-r4n4dfhwH7OzA-k3LqL9636bJD_-a2oDONnZ4Q5c9t1tVLlKFFsMsV8",
}

const oauth2Client = new OAuth2(
    googleConfig.CLIENT_ID,
    googleConfig.SECRED_ID,
    "https://developers.google.com/oauthplayground"
);

oauth2Client.setCredentials({
    refresh_token: googleConfig.REFRESH_TOKEN
});



module.exports.GoogleManager = (function(){

    return {
        oauth2Client: oauth2Client,
        config: googleConfig
    }
})()