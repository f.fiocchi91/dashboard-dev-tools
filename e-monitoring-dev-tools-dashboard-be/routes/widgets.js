var express = require('express');
var router = express.Router();

const FileService = require('../services/fileService').FileService;
const UtilityService = require('../services/utilityService').UtilityService;
const WidgetsService = require('../services/widgetsService').WidgetsService;
const MailService = require('../services/mailService').MailService;

const Widget = require('../classes/widget').Widget;

const mjmlManager = require('../api/mjml').MJMLManager;


const fileService = new FileService();
const utilityService = new UtilityService();
const widgetsService = new WidgetsService();
const mailService = new MailService();

/* GET widgets listing. */
router.get('/', function (req, res, next) {
  res.status(404);
});

router.post('/update', function (req, res, next) {

  
  const widget = new Widget();
  Object.keys(widget).forEach(function (key) {
    if (key === "image") {
      this[key] = req.fields[key];
    } else if (key === "id" || key === "category" || key === "isActive" || key === "order") {
      this[key] = parseInt(req.fields[key]);
    } else {
      this[key] = req.fields[key];
    }

  }, widget);

  widget.clean();
  widgetsService.update(widget).then(function (result) {
    debugger
    result = {
      ok:true,
      message:"Ok"
    };
    if (result.ok === true) {
      res.status(200).send({
        message: result.message,
        data: null
      });
    } else {
      res.status(500).send({
        message: result.message,
        data: null
      });
    }
  });

});

router.post('/add',function(req,res,next){
  widgetsService.add(new Widget());
})

router.post('/image/:id', function (req, res, next) {
  //const fileService = new FileService(); 

  const newImage = "data:image/png;base64," + fileService.convertImage.Encode(req.files.file.path);

  res.status(200).send({
    message: "Ok",
    data: newImage
  });
});

router.get('/data', function (req, res, next) {
  //const fileService = new FileService(); 

  //const data = fileService.widgets();
  widgetsService.data().then(function (snap) {
    debugger
    res.send({
      message: "Ok",
      data: snap.val()
    });

  });

});

router.post('/query', function (req, res, next) {
  //const fileService = new FileService(); 
  const data = JSON.parse(req.fields.data);

  const query = utilityService.generateQuery(data);

  res.status(200).send({
    message: "Ok",
    data: query
  });
});

router.post('/download/queries', function (req, res, next) {

  fileService.generateQueriesFile().then(function(file){

    res.status(200).send(file);

  })

  //res.type('application/octet-stream');
  //res.attachment(file.name);

  

  // res.status(200).send({
  //   message: "Downloaded",
  //   data: file
  // });
})

router.get('/send/mail', function (req, res, next) {

  mjmlManager
    .render()
    .then(
      (response) => {

        mailService.send(response.html)
          .then(
            (r) => {
              res.status(200).send(true);
            },
            (e) => {
              res.status(400).send(false);
            }
          )
      },
      (error) => {
        res.status(500).send({
          message: error
        })
      });



})

module.exports = router;