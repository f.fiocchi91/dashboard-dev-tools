const UtilityService = require('../services/utilityService').UtilityService;
const utilityService = new UtilityService();

module.exports.Widget = (function () {

    function Widget() {
        this.id = null;
        this.title = null;
        this.description = null;
        this.category = null;
        this.image = null;
        this.isActive = null;
        this.uuid = null;
        this.order = null;
    }
    Widget.prototype.clean = function () {
        if (this.image === utilityService.widgetStandardImage === true) {
            this.image = null;
        }
    }
    return Widget;
})()