module.exports.RequestError = (function () {

    function RequestError() {
        Error.call(this);      
        this.status = null;
    }

    return RequestError;
})();
