module.exports.File = (function(){

    function File(name, buffer, type){
        this.buffer = buffer;
        this.name = name;
        this.type = type;
    }
    return File;
})();