module.exports.ResponseMessage = (function () {

    function ResponseMessage() {
        this.ok = false;
        this.message = "Internal Server Error";
        this.data = null;
    }
    // ResponseMessage.prototype.set = function (data) {
    //     this.ok = false;
    //     this.message = "Internal Server Error";
    //     this.data = null;
    // }

    ResponseMessage.prototype.error = function () {
        this.ok = false;
        this.message = "Internal Server Error";
        this.data = null;
    }
    
    return ResponseMessage;
})()