const Convert = require('../helpers/imagesConverter').Convert;
const Query = require('../helpers/queryGenerator').Query;
const fs = require('fs');
const path = require("path");
const File = require('../classes/file').File;
const Stream = require('stream');
const WidgetsService = require('./widgetsService').WidgetsService;
const widgetService = new WidgetsService();

module.exports.FileService = (function () {

    function FileService() {};

    FileService.prototype.convertImage = Convert;

    FileService.prototype.widgets = function () {

        return widgetService.data();

        // const buffer = fs.readFileSync(path.join(process.cwd(), "data", "widgets.json"))
        // return JSON.parse(buffer.toString());
    };

    FileService.prototype.udatewidget = function (widget) {
        const data = {
            message: "Ok",
            data: null,
            ok: true
        }
        try {
            const widgets = this.widgets();

            widgets.forEach((w, index) => {
                if (w.ID === widget.id) {
                    widgets[index].ID = widget.id;
                    widgets[index].Category = widget.category;
                    widgets[index].Image = widget.image;
                    widgets[index].Name = widget.title;
                    widgets[index].Description = widget.description;
                    widgets[index].IsActive = widget.isActive;

                }
            });
            fs.writeFileSync(path.join(process.cwd(), "data", "widgets.json"), JSON.stringify(widgets), 'utf8');

            data.message = "Updated";
        } catch (exception) {
            data.ok = false;
            data.message = "Internal Server Error";
        }

        return data

    }


    FileService.prototype.generateQueriesFile = function () {

        
        return this.widgets().then(function(snap){
            const queries = [];
            var db = snap.val();
            Object.values(db).filter((w) => w.IsActive).forEach((w) => {
                const query = new Query(w.ID, w.Name, w.Description, w.Order, w.Image, w.Category, w.IsActive);
                queries.push(query);
            })
    
            const stringQueries = Query.all(queries);
    
            let text = "";
    
            stringQueries.forEach((q) => {
                text = text + q;
            });
    
            // let buffer = Buffer.from(text);
            // let arraybuffer = Uint8Array.from(buffer).buffer;
            return new File('e-monitoring-images-queries { ' + new Date().toUTCString() + ' }.sql', text, "application/octet-stream");
    

        })
    }

    return FileService;
})();