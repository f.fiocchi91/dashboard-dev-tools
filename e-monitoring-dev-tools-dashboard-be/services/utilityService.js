const Query = require('../helpers/queryGenerator').Query;
const standardImage = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNTAiIGhlaWdodD0iMjUwIiB2aWV3Qm94PSIwIDAgMjQgMjQiPjxwYXRoIGQ9Ik0xOSA3djIuOTlzLTEuOTkuMDEtMiAwVjdoLTNzLjAxLTEuOTkgMC0yaDNWMmgydjNoM3YyaC0zem0tMyA0VjhoLTNWNUg1Yy0xLjEgMC0yIC45LTIgMnYxMmMwIDEuMS45IDIgMiAyaDEyYzEuMSAwIDItLjkgMi0ydi04aC0zek01IDE5bDMtNCAyIDMgMy00IDQgNUg1eiIvPjxwYXRoIGQ9Ik0wIDBoMjR2MjRIMHoiIGZpbGw9Im5vbmUiLz48L3N2Zz4=";
module.exports.UtilityService = (function(){

    function UtilityService(){
        this.widgetStandardImage = standardImage
    }
    UtilityService.prototype.generateQuery = function(data){
        if(data.image === this.widgetStandardImage){
            data.image = null;
        }
        const query = new Query(data.id,data.title,data.description,data.order,data.image,data.category,data.isActive);
        return query.generate();
    }

    return UtilityService;
})()