const db = require('../api/firebase').db
const firebase = require('../api/firebase').firebase;

module.exports.WidgetsService = (function () {

    const uuidv4 = function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
      }

    function WidgetsService() {
        
    }

    WidgetsService.prototype.data = function () {
        return db.ref('/widgets').once('value');
    }

    WidgetsService.prototype.add = function (widget) {
        db.ref('widgets/' + uuidv4()).set({
            ID: 999999,
            Name: "NEW WIDGET",
            Description: "NEW WIDGET",
            Category: null,
            Image: null,
            IsActive: 0,
            Order: null
        }, (response) => {
            console.log(response)
            debugger
        });
    }

    WidgetsService.prototype.update = function (widget) {
        debugger
        return db.ref('widgets/' + widget.uuid).set({
            ID: widget.id,
            Name: widget.title,
            Description: widget.description,
            Category: widget.category,
            Image: widget.image,
            IsActive: widget.isActive,
            Order:widget.order
        })
    }

    WidgetsService.prototype.delete = function (id) {

    }

    return WidgetsService;
})()