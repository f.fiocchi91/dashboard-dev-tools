const standardImage = "PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNTAiIGhlaWdodD0iMjUwIiB2aWV3Qm94PSIwIDAgMjQgMjQiPjxwYXRoIGQ9Ik0xOSA3djIuOTlzLTEuOTkuMDEtMiAwVjdoLTNzLjAxLTEuOTkgMC0yaDNWMmgydjNoM3YyaC0zem0tMyA0VjhoLTNWNUg1Yy0xLjEgMC0yIC45LTIgMnYxMmMwIDEuMS45IDIgMiAyaDEyYzEuMSAwIDItLjkgMi0ydi04aC0zek01IDE5bDMtNCAyIDMgMy00IDQgNUg1eiIvPjxwYXRoIGQ9Ik0wIDBoMjR2MjRIMHoiIGZpbGw9Im5vbmUiLz48L3N2Zz4=";
const image = "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNTAiIGhlaWdodD0iMjUwIiB2aWV3Qm94PSIwIDAgMjQgMjQiPjxwYXRoIGQ9Ik0wIDBoMjR2MjRIMHoiIGZpbGw9Im5vbmUiLz48cGF0aCBkPSJNMyAxM2g4VjNIM3YxMHptMCA4aDh2LTZIM3Y2em0xMCAwaDhWMTFoLTh2MTB6bTAtMTh2Nmg4VjNoLTh6Ii8+PC9zdmc+";

const nodemailer = require('nodemailer');
const googleAPI = require('../api/google').GoogleManager;


module.exports.MailService = (function () {

    function MailService() {}
    MailService.compile = function(template){
        let compiled = '';
        compiled = compiled + template
        compiled = compiled.replace("{{MAIN_TITLE}}",'Queries');
        compiled = compiled.replace("{{IMAGE}}",image);
        compiled = compiled.replace("{{APP_NAME}}",'Dashboard DEV Tools');
        compiled = compiled.replace("{{MAIN_TEXT}}",'Updated queries ' + Date.now() );
        compiled = compiled.replace("{{BUTTON_TEXT}}",'Download');
        compiled = compiled.replace("{{BUTTON_URL}}",'google.com');
        compiled = compiled.replace("{{SECONDARY_TEXT}}",'Thanks');
        compiled = compiled.replace("{{FOOTER_TEXT}}",'Francesco');

        return compiled;

    };
     MailService.prototype.send = async function (template) {
        
        return googleAPI.oauth2Client.getAccessToken().then((response)=>{
            
            const transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 465,
                secure: true,
                auth: {
                    type: "OAuth2",
                    user: "f.fiocchi91@gmail.com",
                    clientId: googleAPI.config.CLIENT_ID,
                    clientSecret: googleAPI.config.SECRED_ID,
                    refreshToken: googleAPI.config.REFRESH_TOKEN,
                    accessToken: response.token
                }
            });
            debugger
            const compiledTemplate = MailService.compile(template);

            const mailOptions = {
                from: 'Dashboard DEV Tools <f.fiocchi91@gmail.com>',
                to: ["francesco.fiocchi@capgemini.com", "francesco.face@hotmail.it"],
                subject: 'Updated Queries',
                html: compiledTemplate,
                attachments: []
            };
    
            return new Promise((resolve, reject) => {
                
                    transporter.sendMail(mailOptions, function (error, info) {
                        debugger
                        if (error) {
                            console.log(error);
                            //stream.close();
                            reject(false);
                        } else {
                            console.log('Email sent: ' + info.response);
                            resolve(true);
                        }
                    })
                
            })
        },(e)=>{ return new Promise((resolve, reject) => { reject(false); }) })

    }

    return MailService;
})()