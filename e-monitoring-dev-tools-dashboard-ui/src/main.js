import Card from './classes/card'
import Widget from './classes/widget';
import routes from './helpers/routes'
import List from 'list.js';
import Modal from './classes/modal';
import Utils from './helpers/utils'
import './classes/widget';
import Search from './classes/search';

import navbar from './classes/navbar';

Toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-bottom-center",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "3000000000",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

navbar.init();

window.codeModal = new Modal("js-code-modal", "SQL Code");
window.codeModal.init();

fetch(routes.widgets.data, {
    method: "GET"
}).then((response) => {
    return response.json().then((data) => {
        return data
    })
}, () => {
    Toastr.error("Technical Error")
}).then((dataFromServer) => {

    const data = [];

    Object.keys(dataFromServer.data).forEach(key => {
        let w = new Widget();
        let element = dataFromServer.data[key];
        data.push(element);
        w.uuid = key;
        w.id = element.ID;
        w.title = element.Name;
        w.description = element.Description;
        w.image = "data:image/png;base64," + element.Image;
        //w.image = upload;
        w.category = element.Category;
        w.order = element.Order;
        w.isActive = parseInt(element.IsActive);
        debugger
        const card = new Card(w.id, w, key);
        card.init();
        card.category(w.category);
        card.bindEvents();

        card.dropzone();



    });

    jQuery(".spinner-border-container").hide();
    navbar.show();
    var options = {
        valueNames: [{
                data: ['id']
            },
            {
                data: ['name']
            }
        ]
    };

    window.inputSearch = new Search('search-cards', data);
    window.inputSearch.init();

    window.cards = new List('widgets-list', options);
    document.querySelector("#search-card").addEventListener("keyup", function (event) {

        cards.search(event.target.value)
    })


});

window.cardList = null;

console.log("v.1.0.4");