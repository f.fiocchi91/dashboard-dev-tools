//const WidgetCategory = {"Customer":1,"Financials":2,"RIO":3,"Challenges":4,"CVRevenue":5,"RevenueInvoice":6,"CostsRevenue":7,"WIPRevenue":8,"DVI":9,"SureLikelyCM":10,"PercentageCM":11,"PredicatabilityInvoices":12,"PredicatabilityRevenue":13,"PredicatabilityCosts":14,"PredicatabilityMargin":15,"SalesDelivery":16,"NewCustomer":17,"HumanResources":18,"EmDeliveryStatus":19,"Contract":20,"FinanceStatus":21,"FinanceDetails":22,"People":23,"Cybersecurity":24,"GDPR":25,"Quality":26,"ManagementStatus":27,"SafetyCritical":28,"BusinessContinuityCritical":29,"DVIFollowUp":30, "KPIs":31, "Milestones":32}

const WidgetCategory = {
    FinanceIndicators:1,
    MonthlyStatusSummary:2,
    DeliveryTargetsAndIndicators:3,
    FinanceChecks:4,
    EMMX:5,
    Predictability:6,
    WorkUnitsAnalysis:7
}
const Datamapping = {
    1:"Finance Indicators",
    2:"Monthly Status Summary",
    3:"Delivery Targets And Indicators",
    4:"Finance Checks",
    5:"EMMX",
    6:"Predictability",
    7:"WorkUnits Analysis",

}
const categories = [];
Object.keys(WidgetCategory).forEach(function (key) {
    categories.push({
        name: Datamapping[this[key]],
        category: this[key]
    })
}, WidgetCategory);

export default categories;




