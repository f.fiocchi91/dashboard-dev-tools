import widgetsCategory from '../enums/widgetCategory';
import cardTemplate from '../templates/cardTemplate.handlebars';
import Widget from './widget';
import $ from 'jquery';
import 'bootstrap';
import upload from 'bootstrap-icons/icons/upload.svg';
import add_image from '../images/add_photo_alternate-24px.svg';
import routes from '../helpers/routes';

const parser = new DOMParser();

export default function Card(id, data, key) {
    this.element = null;
    this.id = id;
    this.data = data;
    this.form = null;
    this.uuid = key;
};
Card.prototype.success = function () {
    $(this.element).find(".card").addClass("border-success");

    setTimeout(function (event) {
        $(this.element).find(".card").removeClass("border-success");
        $(this.element).find(".card").removeClass("ondrag");
    }.bind(this), 3000)
};
Card.prototype.error = function () {
    $(this.element).find(".card").addClass("border-danger");
    setTimeout(function (event) {
        $(this.element).find(".card").removeClass("border-danger");
        $(this.element).find(".card").removeClass("ondrag");
    }.bind(this), 3000)
};
Card.prototype.init = function () {
    this.element = parser.parseFromString(cardTemplate({
        widget: this.data,
        categories: widgetsCategory
    }), "text/html").querySelector(".card").parentElement;
    document.querySelector(".container-fluid .row").append(this.element);

    this.form = this.element.querySelector("form");

    $(this.element.querySelectorAll('[data-toggle="tooltip"]')).tooltip();
    this.removeImage();

};
Card.prototype.category = function (category) {
    //this.element.querySelector("select").value = category;
    this.form.category.value = category;

};
Card.prototype.bindEvents = function () {

    this.element.addEventListener("submit", function (event) {
        this.update(event);
    }.bind(this));

    this.element.addEventListener("change", function (event) {
        this.onchange(event);
    }.bind(this));

    this.element.querySelector('[data-type="query"]').addEventListener("click", function (event) {
        this.generateQuery(event)
    }.bind(this));

    this.element.querySelector("img").addEventListener("error", function (event) {
        this.src = add_image;
    })

    this.element.querySelector('input').form.file.addEventListener("change", function (event) {
        this.updateImage();
    }.bind(this));
};
Card.prototype.dropzone = function () {
    this.element.querySelector(".card").addEventListener("drop", function (event) {
        event.preventDefault();
        const form = new FormData();
        form.append("file", event.dataTransfer.items[0].getAsFile());

        fetch(routes.widgets.image + this.id, {
                method: "POST",
                body: form
            })
            .then((response) => {
                if (response.ok === true) {
                    return response.json();
                } else {
                    return {
                        ok: response.ok,
                        message: response.statusText
                    }
                }

            })
            .then((json) => {
                if (json.ok === false) {
                    debugger
                    this.error();
                    Toastr.error(json.message);
                } else {
                    jQuery(this.element).find(".card").removeClass("ondrag");
                    jQuery(this.element).find(".card").removeClass("border-danger");

                    this.success();
                    this.element.querySelector("img").src = json.data;
                }


            })
    }.bind(this));
    this.element.querySelector(".card").addEventListener("dragover", function (event) {
        event.preventDefault();
        jQuery(this).addClass("ondrag");
    });

    this.element.querySelector(".card").addEventListener("dragleave", function (event) {
        //event.preventDefault();
        //this.style.border = "1px solid rgba(0,0,0,.125)"; this.style.outline = "0"; this.style.boxShadow = "0";
        event.preventDefault();
        jQuery(this).removeClass("ondrag");

    });

};
Card.prototype.generateQuery = function () {
    this.onchange();
    const formData = new FormData();
    formData.append("data", JSON.stringify(this.data));


    fetch(routes.widgets.query, {
        method: "POST",
        body: formData
    }).then((response) => {

        return response.json();
    }).then((json) => {

        window.codeModal.text = json.data;
        window.codeModal.show();
        window.codeModal.content('<code class="prettyprint">' + json.data + '</code>');
        //$0.scrollTo(0, $0.scrollHeight)
        PR.prettyPrint();

        Toastr.success(json.message)
    })
};
Card.prototype.update = function(event){
    event.preventDefault();
    debugger
    const form = new FormData(event.target);
    form.append("isActive", this.data.isActive)
    this.data.image = this.element.querySelector("img").src.split(",")[1];
    form.append("image", this.data.image);
    form.append("uuid", this.uuid);
    const card = this;
    fetch(routes.widgets.update, {
            method: "POST",
            body: form
        })
        .then((response) => {
            debugger
            return response.json()
        }, (error) => {
            card.error();
            Toastr.error('Technical Error');
        })
        .then((json) => {
            card.success();
            Toastr.success(json.message);
        })

};
Card.prototype.updateImage = function (event) {
    const formData = new FormData(this.form);
    formData.set("order", this.data.order);
    formData.set("isActive", this.data.isActive);
    const card = this;

    fetch(routes.widgets.image + this.id, {
            method: "POST",
            body: formData
        })
        .then((response) => {
            return response.json()
        })
        .then((jsonFromServer) => {
            Toastr.success(jsonFromServer.message);
            card.element.querySelector("img").src = jsonFromServer.data;
        }, (error) => {
            card.element.querySelector("form").file.value = null;
            Toastr.error('Technical Error');
        });
};
Card.prototype.onchange = function (event) {
    debugger
    this.data.id = parseInt(this.form.id.value);
    this.data.title = this.form.title.value;
    this.data.description = this.form.description.value;
    this.data.isActive = this.form.isactive.checked === true ? 1 : 0;
    this.data.category = parseInt(this.form.category.value);
    this.data.order = parseInt(this.form.order.value);
    this.data.image = this.element.querySelector("img").src.split(",")[1];

};
Card.prototype.removeImage = function () {
    this.form.file.value = null;
    this.element.querySelector('[data-action="delete-image"]').addEventListener("click", function (event) {

        this.element.querySelector("img").src = null;
        this.form.file.value = null;
    }.bind(this))
};