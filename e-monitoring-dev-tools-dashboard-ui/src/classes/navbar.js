import navbar from '../templates/navbar.handlebars';
import devlogo from '../images/dev.svg';
import logo from '../images/dashboard-24px.svg';
import Utils from '../helpers/utils';
import routes from '../helpers/routes'
const parser = new DOMParser();
const navbarObj = {
    image: null
}
debugger
if (process.env.NODE_ENV === "production") {
    navbarObj.image = logo;
} else {
    navbarObj.image = devlogo;
}

const Navbar = (function () {

    const _init = function () {
        const navString = navbar(navbarObj);
        const page = parser.parseFromString(navString, "text/html");
        const nav = page.querySelector("nav");
        document.body.prepend(nav)

        _hide();

        nav.querySelector("#js-download-queries").addEventListener("click", function (event) {


            fetch(routes.widgets.download.queries, {
                    method: "POST"
                })
                .then((response) => {

                    return response.json()

                }).then((file) => {

                    //Utils.DownloadFileFromJSONFile(file.buffer, file.name, file.type);
                    Utils.DownloadFile(file.buffer, file.name, file.type, false, true);
                })

        })

        nav.querySelector("#js-add-new-widget").addEventListener("click", function (event) {


            fetch(routes.widgets.add, {
                    method: "POST"
                })
                .then((response) => {

                    return response.json()

                }).then((file) => {


                })

        })


    }

    const _show = function () {
        jQuery("nav").show();
    }
    const _hide = function () {
        jQuery("nav").hide();
    }

    return {
        init: _init,
        show: _show,
        hide: _hide
    }

})()
export default Navbar