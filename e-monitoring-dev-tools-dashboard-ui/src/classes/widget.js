export default function Widget() {
    this.uuid = null;
    this.id = null;
    this.title = null;
    this.description = null;
    this.category = null;
    this.image = null;
    this.order = null;
    this.isActive = null;
}
