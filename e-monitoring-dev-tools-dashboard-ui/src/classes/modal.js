//import $ from 'jquery';
//import 'bootstrap'
const parser = new DOMParser();

export default function Modal(id,title) {
    this.id = id;
    this.title = title;
    this.titleid = title +"-"+ id;
    this.body = null;
    this.element = null;
    this.text = null;
}


Modal.prototype.init = function (content) {
    this.body = content;
    const doc = parser.parseFromString(
        '<div class="modal fade" id="' + this.id + '" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="' + this.titleid + '" aria-hidden="true">' +
        '  <div class="modal-dialog" role="document">' +
        '    <div class="modal-content">' +
        '      <div class="modal-header">' +
        '        <h5 class="modal-title" id="' + this.titleid + '">' + this.title + '</h5>' +
        '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
        '          <span aria-hidden="true">&times;</span>' +
        '        </button>' +
        '      </div>' +
        '      <div class="modal-body">' +
        '        <div>' + this.body + '</div>' +
        '      </div>' +
        '      <div class="modal-footer">' +
        '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
        //'        <button type="button" class="btn btn-primary">Understood</button>' +
        '      </div>' +
        '    </div>' +
        '  </div>' +
        '</div>',
        "text/html");
        const modal = doc.querySelector(".modal");
        document.querySelector("body").append(modal);
        this.element = jQuery(modal);
};

Modal.prototype.content = function(content){
const doc = parser .parseFromString(content,"text/html");
jQuery(this.element[0].querySelector(".modal-body")).children().remove();
this.element[0].querySelector(".modal-body").append(doc.querySelector("body").firstElementChild);

this.clipboardButton();
};

Modal.prototype.show = function(){
    this.element.modal("show");
};
Modal.prototype.hide = function(){
    this.element.modal("hide");
};

Modal.prototype.clipboardButton = function(){
debugger
    const doc = parser.parseFromString('<div class="bd-clipboard"><button type="button" class="btn-clipboard" data-toggle="tooltip" title="" data-original-title="Copy to clipboard">Copy</button></div>',"text/html");

    const button = doc.querySelector(".bd-clipboard");
    debugger
    this.element[0].querySelector(".modal-body").firstElementChild;
    this.element[0].querySelector(".modal-body").prepend(button);
    jQuery(button).find('[data-toggle="tooltip"]').tooltip({boundary:"viewport"});

    button.querySelector("button").addEventListener("click",function(event){
        debugger
        navigator.clipboard.writeText(this.text).then(function(response){
            
            jQuery(button).find('[data-toggle="tooltip"]').data("bs.tooltip").tip.querySelector(".tooltip-inner").innerText = "Copied!"
            debugger
            jQuery(button).find('[data-toggle="tooltip"]').data("bs.tooltip")._popper.update()
        })
    
    }.bind(this));
};
