import List from  'list.js'; 

const parser = new DOMParser();
export default function Search(id, data) {
    this.input = document.querySelector("#" + id + " input");
    this.id = id;
    this.dropdown = document.querySelector("#" + id + " .dropdown-menu");
    this.data = data;
    this.list = null;

};

Search.prototype.init = function () {
    const d = this.dropdown;
    this.data.forEach(w => {
        const i = new Item(w.ID, w.Name);
        d.append(i.element);
        i.addclick();
    });

    var options = {
        valueNames: [{
                data: ['id']
            },
            {
                data: ['name']
            }
        ]
    };
    this.list = new List('search-cards', options);
    
    this.input.addEventListener("keyup", function (event) {
        if (event.target.value.length >= 3) {
            $(this.dropdown).addClass("show");
        } else {
            $(this.dropdown).removeClass("show");
        }
    }.bind(this))
    this.input.addEventListener("keyup", function (event) {
        debugger
        this.list.fuzzySearch(event.target.value);

    }.bind(this))
    this.input.addEventListener("focus", function (event) {
        if (event.target.value.length >= 3) {
            $(this.dropdown).addClass("show");
        } else {
            $(this.dropdown).removeClass("show");
        }
    }.bind(this))
    this.input.addEventListener("blur", function (event) {
        setTimeout(function(){
            $(this.dropdown).removeClass("show");
        }.bind(this),150)
        
    }.bind(this))
    
    this.dropdown.addEventListener("click",function (event) {
        this.input.value = event.target.dataset.name;
        
        this.input.dispatchEvent(new Event("keyup"))
    }.bind(this))
}

function Item(id, name) {
    this.id = id;
    this.name = name;
    this.element = generate(id, name);
}

Item.prototype.addclick = function(){
    
}

const generate = (id, name) => {
    const page = parser.parseFromString('<a data-name="' + name + '" data-id="' + id + '"class="dropdown-item" href="#">' + name + '</a>', "text/html");
    const a = page.querySelector("a");
    
    return page.querySelector("a");
}