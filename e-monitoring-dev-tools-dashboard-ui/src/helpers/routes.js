console.log(process.env.NODE_ENV)
const origin = (() => {
    if (process.env.NODE_ENV === "production") {
        return "https://dashboard-dev-tools-core.onrender.com/";
    } else {
        return "http://localhost:3000/";
    }
})();

const routes = {
    widgets: {
        data: origin + "widgets/data/",
        query: origin + "widgets/query/",
        download:{
            queries: origin + "widgets/download/queries"
        },
        add: origin + "widgets/add/",
        update: origin + "widgets/update/",
        image: origin + "widgets/image/",
        send: {
            mail: origin + "widgets/send/mail"
        },
    }

};

export default routes
