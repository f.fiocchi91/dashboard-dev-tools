const downloadFile = function (base64EncodedFileData, fileName, fileType, isBinaryFile, isAlreadyBase64DecodedString) {
    if (isAlreadyBase64DecodedString !== true) {
        base64EncodedFileData = atob(base64EncodedFileData);
    }

    if (isBinaryFile === true) {
        const bytes = new Array(base64EncodedFileData.length);
        for (var i = 0; i < base64EncodedFileData.length; i++) {
            bytes[i] = base64EncodedFileData.charCodeAt(i);
        }
        base64EncodedFileData = new Uint8Array(bytes);
    }


    const blob = new Blob([base64EncodedFileData], {
        type: fileType
    });
    //const blob = new Blob(base64EncodedFileData, { type: fileType });
    const objectURL = window.URL.createObjectURL(blob);
    const downloaderAnchor = document.createElement('a');

    // don't remove to download file with useful name
    downloaderAnchor.href = objectURL;
    downloaderAnchor.download = fileName;
    downloaderAnchor.click();

    URL.revokeObjectURL(objectURL);

    // if (isIE === true) {
    //     window.BlobBuilder = window.BlobBuilder ||
    //         window.WebKitBlobBuilder ||
    //         window.MozBlobBuilder ||
    //         window.MSBlobBuilder;

    //     const blobBuilder = new BlobBuilder();
    //     blobBuilder.append(base64EncodedFileData);
    //     const blob = blobBuilder.getBlob(fileType);

    //     window.navigator.msSaveOrOpenBlob(blob, fileName);
    // } else {
    //     const blob = new Blob([base64EncodedFileData], { type: fileType });
    //     const objectURL = window.URL.createObjectURL(blob);
    //     const downloaderAnchor = document.createElement('a');

    //     // don't remove to download file with useful name
    //     downloaderAnchor.href = objectURL;
    //     downloaderAnchor.download = fileName;
    //     downloaderAnchor.click();

    //     URL.revokeObjectURL(objectURL);
    // }
};

const downloadFileFromJSONFile = function (buffer, name, type) {
    const blob = new Blob([buffer], {
        type: type
    });
    //const blob = new Blob(base64EncodedFileData, { type: fileType });
    const objectURL = window.URL.createObjectURL(blob);
    const downloaderAnchor = document.createElement('a');

    // don't remove to download file with useful name
    downloaderAnchor.href = objectURL;
    downloaderAnchor.download = name;
    downloaderAnchor.click();

    URL.revokeObjectURL(objectURL);
}
const Utils = {};

Utils.DownloadFile = downloadFile;
Utils.DownloadFileFromJSONFile = downloadFileFromJSONFile;

export default Utils;