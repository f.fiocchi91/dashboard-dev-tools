const path = require('path');
const webpack = require('webpack');

/*
 * SplitChunksPlugin is enabled by default and replaced
 * deprecated CommonsChunkPlugin. It automatically identifies modules which
 * should be splitted of chunk by heuristics using module duplication count and
 * module category (i. e. node_modules). And splits the chunks…
 *
 * It is safe to remove "splitChunks" from the generated configuration
 * and was added as an educational example.
 *
 * https://webpack.js.org/plugins/split-chunks-plugin/
 *
 */

const HtmlWebpackPlugin = require('html-webpack-plugin');

/*
 * We've enabled HtmlWebpackPlugin for you! This generates a html
 * page for you when you compile webpack, which will make you start
 * developing and prototyping faster.
 *
 * https://github.com/jantimon/html-webpack-plugin
 *
 */

const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: {
        main: path.resolve(__dirname, 'src/main.js'),
        vendor: path.resolve(__dirname, 'src/vendor.js')
    },

    output: {
        filename: '[name].[chunkhash].js',
        path: path.resolve(__dirname, 'dist')
    },

    plugins: [
        new webpack.ProgressPlugin(),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src/main.html'),
            title: 'Dashboard DEV Tools'
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            Popper: ['popper.js', 'default'],
            Toastr: 'toastr'
        }),
        // new webpack.DefinePlugin({
        //     "process.env": {
        //         // defaults the environment to development if not specified
        //         "NODE_ENV": JSON.stringify(process.env.NODE_ENV)
        //     }
        // }),
    ],

    module: {
        rules: [{
                test: /.(js|jsx)$/,
                include: [path.resolve(__dirname, 'src')],
                loader: 'babel-loader',

                options: {
                    plugins: ['syntax-dynamic-import'],

                    presets: [
                        [
                            '@babel/preset-env',
                            {
                                modules: false
                            }
                        ]
                    ]
                }
            },
            
            {
                test: /\.handlebars$/,
                loader: "handlebars-loader",
                query: {
                    partialDirs: [
                        path.join(__dirname, 'src', 'templates', 'partials')
                    ]
                }
            },
            {
                test: /\.(png|jpg|gif|svg)$/i,
                use: [
                    'url-loader'
                ],
            },
            {
                test: /\.html$/,
                use: [{
                    loader: 'html-loader',
                    options: {
                        //minimize: true,
                    },
                    }],
            },
        ]
    },



    // devServer: {
    // 	open: true
    // }

};